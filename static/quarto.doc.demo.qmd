---
title: "Quarto Document Demo"
author: "Kno"
format: html
editor: visual
---

## Quarto

-   Rstudio
-   Project --\> Quarto document/R notebook
-   Naming, Saving, Organizing and Backing up
-   Teaming: Github/Gitlab, Telegram/Discord
-   [Literate Programming](https://en.wikipedia.org/wiki/Literate_programming): [Markdown](https://www.markdownguide.org/), [Quarto](https://quarto.org/docs/get-started/hello/rstudio.html)

## Running Code

When you click the **Render** button a document will be generated that includes both content and the output of embedded code. You can embed code like this:

```{r}
1 + 1
```

You can add options to executable code like this

```{r}
#/ echo: false
2 * 2
```

The `echo: false` option disables the printing of code (only output is displayed).

## [Tutorial: Hello, Quarto](https://quarto.org/docs/get-started/hello/rstudio.html)

### Read and Links

- [Ecological Sexual Dimorphism and Environmental Variability within a Community of Antarctic Penguins (Genus *Pygoscelis*)](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0090081) <data source>
- [A farewell to the sum of Akaike weights](https://besjournals.onlinelibrary.wiley.com/doi/10.1111/2041-210X.12835) <parameter likelihood>
