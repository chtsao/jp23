---
title: "Quarto"
date: 2023-03-09T08:57:02+08:00
draft: false
categories: [notes]
---
<img src="https://upload.wikimedia.org/wikipedia/commons/8/8c/Folio-Quarto-Octavo_Compared.svg" style="zoom:10%;" />

### Quarto 
* [Welcome to Quarto](https://quarto.org/)
* quarto.doc.demo ([html](https://chtsao.gitlab.io/jp23/quarto.doc.demo.html), [qmd](https://chtsao.gitlab.io/jp23/quarto.doc.demo.qmd))


### Quarto Example

* A Quick Guide to Machine Learning
[html](http://faculty.ndhu.edu.tw/~chtsao/qresent/qml01.html#/title-slide), 
[qmd](http://faculty.ndhu.edu.tw/~chtsao/qresent/qml01.qmd)