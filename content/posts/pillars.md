---
title: "Pillars"
date: 2023-03-16T07:01:09+08:00
draft: false
categories: [notes]
---

![](https://media.geeksforgeeks.org/wp-content/cdn-uploads/20201204170056/4-Key-Pillars-of-Data-Science.png)

* [4 Key Pillars of Data Science](https://www.geeksforgeeks.org/4-key-pillars-of-data-science/)

## Domain Knowledge
* Learning
  * [To Be a Great Leader, You Need the Right Mindset ](https://hbr.org/2020/01/to-be-a-great-leader-you-need-the-right-mindset)
  * [Seven essential elements of a lifelong-learning mind-set](https://www.mckinsey.com/capabilities/people-and-organizational-performance/our-insights/seven-essential-elements-of-a-lifelong-learning-mind-set)


## Math Skills

* Calculus
* Linear Algebra/Matrix Theory
* Numerical Analysis
  * [TeaTime Numerical Analysis](https://raw.githubusercontent.com/lqbrin/tea-time-numerical/master/TeaTimeNumericalAnalysis.pdf)

* General Linear Model Textbook: Textbook: Kutner, M.H., Nachtsheim, C.J., Neter, J. and Li, W. (2005). Applied Linear Statistical Models, 5th edition. McGraw-Hill. [Kno's APLM](http://faculty.ndhu.edu.tw/~chtsao/edu/11/aplm/aplm11.html)
* SML Textbook: [Hastie, Tibshirani and Friedman (2009). The Elements of Statistical Learning: Data Mining, Inference and Prediction.](https://hastie.su.domains/ElemStatLearn/) 2nd Edition. (aka. ESLII)  Springer-Verlag. Or [Legal downloadable from NDHU](http://134.208.29.176:8080/toread/opac/bibliographic_view?NewBookMode=false&id=844743&location=0&q=Elements+of+Statlstical+Machine+Learning&start=0&view=CONTENT); [Kno's SML 2022](https://chtsao.gitlab.io/sml22/)
* AMSA Textbook: Hardle and Simar (2015). Applied Multivariate Statistical Analysis, 4th Edition, Springer. (@[Springer](https://link.springer.com/book/10.1007%2F978-3-662-45171-7), [Extras](http://extras.springer.com/2015/978-3-662-45170-0), [Quantlet](http://www.quantlet.de/), [MVA@Github](https://github.com/QuantLet/MVA/), [Webbook](http://sfb649.wiwi.hu-berlin.de/fedc_homepage/xplore/ebooks/html/mva/mvahtml.html), [Data sets](https://www2.karlin.mff.cuni.cz/~hlavka/sms2/index.html)); [Kno's AMSA  2021](https://chtsao.gitlab.io/amsa21/)

## CS Skills

* [Kno's Rgames 2022](https://chtsao.gitlab.io/rgame22/)

## Communication Skills
Learning with a Output Mindset